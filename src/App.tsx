import React from 'react';

import './App.css';
import HelloWorld from './helloworld/helloworld';

function App() {
  return (
    <div className="App">
     <HelloWorld value={"Alexandre"}></HelloWorld>
    </div>
  );
}

export default App;
