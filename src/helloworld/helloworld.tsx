import * as React from 'react';

export interface IHelloWorldProps {

  value: string;

}

export default class HelloWorld extends React.Component<IHelloWorldProps> {
  public render() {
    return (
      <div className='text-2xl text-gray-800'>
        Bonjour {this.props.value}
      </div>
    );
  }
}

